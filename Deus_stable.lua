-- @needed lib..// ---

local notify = require "notify"
local clipboard = require("clipboard")

math.lerp = function(a, b, percentage)
    return a + (b - a) * percentage
end
color_t.fade_color = function(f, s, a)
    local r = math.lerp(f.red * 255, s.red * 255, a)
    local g = math.lerp(f.green * 255, s.green * 255, a)
    local b = math.lerp(f.blue * 255, s.blue * 255, a)
    local a = math.lerp(f.alpha * 255, s.alpha * 255, a)
    return color_t.new(r, g, b, a)
end
renderer.text_outline = function(text, font, pos, size, color)
    renderer.text(text, font, vec2_t.new(pos.x - 1, pos.y - 0), size, color)
    renderer.text(text, font, vec2_t.new(pos.x - 0, pos.y - 1), size, color)
    renderer.text(text, font, vec2_t.new(pos.x + 1, pos.y + 0), size, color)
    renderer.text(text, font, vec2_t.new(pos.x + 0, pos.y + 1), size, color)
    renderer.text(text, font, vec2_t.new(pos.x - 1, pos.y - 1), size, color)
    renderer.text(text, font, vec2_t.new(pos.x + 1, pos.y + 1), size, color)
    renderer.text(text, font, vec2_t.new(pos.x + -1, pos.y + 1), size, color)
    renderer.text(text, font, vec2_t.new(pos.x - -1, pos.y - 1), size, color)
end
function DrawShadowedText(shadow, text, font, pos, size, color)
    renderer.text(text, font, vec2_t.new(pos.x + 1, pos.y + 1), size, color_t.new(0, 0, 0, 255))
    renderer.text(text, font, vec2_t.new(pos.x, pos.y), size, color)
end

local SCRIPT_NAME = client.get_script_name()

local whitelist = {
    "ExclusiveHeaven",
    "skipass1337",
	"TheKreyz1337",
    "rolikprefer"
}

function DrawEnchantedText(speed, text, font, pos, size, color, glow_color)
    local chars_x = 0
    local len = #text - 1
    for i = 1, len + 1 do
        local text_sub = string.sub(text, i, i)
        local text_size = renderer.get_text_size(font, size, text_sub .. "")
        local color_glowing = color_t.fade_color(glow_color, color, math.abs(math.sin((globalvars.get_real_time() - (i * 0.02)) *1.5)))
        renderer.text(text_sub .. "", font, vec2_t.new(pos.x + chars_x, pos.y), size, color_glowing)
        chars_x = chars_x + text_size.x
    end
end
function DrawFadingText(speed, text, font, pos, size, color, fading_color)
    local color_fade = color_t.fade_color( color, fading_color, math.abs(math.sin((globalvars.get_real_time() - 0.08) * speed)))
    renderer.text(text, font, vec2_t.new(pos.x, pos.y), size, color_fade)
end

local col1 = color_t.new(0, 0, 0, 255)
local col2 = color_t.new(255, 255, 255, 255)
local next_col = 0
function DrawRainbowText(speed, text, font, pos, size)
    next_col = next_col + 1 / (100 / speed)
    if next_col >= 1 then
        next_col = 0
        col1 = col2
        col2 = color_t.new(math.random(0, 255), math.random(0, 255), math.random(0, 255), 255)
    end
    local color_fade = color_t.fade_color(col1, col2, next_col)
    renderer.text(text, font, vec2_t.new(pos.x, pos.y), size, color_fade)
end


function DrawGlowingText(static, text, font, pos, size, color, glow_color)
    local initial_a = 20
    local a_by_i = 2
    local alpha_glow = math.abs(math.sin((globalvars.get_real_time() - 0.1) * 2))
    if static then alpha_glow = 1 end
    for i = 1, 5 do
        renderer.text_outline(text, font, pos, size, color_t.new(glow_color.red * 255, glow_color.green * 255, glow_color.blue * 255, ((initial_a - (i * a_by_i)) * alpha_glow)))
    end
    renderer.text(text, font, pos, size, color)
end

function DrawBouncingText(style, intesity, text, font, pos, size, color)
    local chars_x = 0
    local len = #text - 1
    for i = 1, len + 1 do
        local text_sub = string.sub(text, i, i)
        local text_size = renderer.get_text_size(font, size, text_sub .. "")
        local y_pos = 1
        local mod = math.sin((globalvars.get_real_time() - (i * 0.1)) * (2 * intesity))
        if style == 1 then
            y_pos = y_pos - math.abs(mod)
        elseif style == 2 then
            y_pos = y_pos + math.abs(mod)
        else
            y_pos = y_pos - mod
        end
        renderer.text(text_sub .. "", font, vec2_t.new(pos.x + chars_x, pos.y - (5 * y_pos)), size, color)
        chars_x = chars_x + text_size.x
    end
end

local next_electic_effect = globalvars.get_current_time() + 0
local electric_effect_a = 0
function DrawElecticText(intensity, text, font, pos, size, color)
    local text_size = renderer.get_text_size(font, size, text)
    renderer.text(text, font, pos, size, color)
    if electric_effect_a > 0 then
        electric_effect_a = electric_effect_a - (1000 * globalvars.get_frame_time())
    end
    for i = 1, math.random(0, 5) do
        line_x = math.random(0, text_size.x)
        line_y = math.random(0, text_size.y)
        line_x2 = math.random(0, text_size.x)
        line_y2 = math.random(0, text_size.y)
        renderer.line(vec2_t.new(pos.x + line_x, pos.y + line_y), vec2_t.new(pos.x + line_x2, pos.y + line_y2), color_t.new(102, 255, 255, electric_effect_a))
    end
    local effect_min = 0.5 + ( 1 - intensity )
    local effect_max = 1.5 + ( 1 - intensity )
    if next_electic_effect <= globalvars.get_current_time() then
        next_electic_effect = globalvars.get_current_time() + math.random( effect_min, effect_max )
        electric_effect_a = 255
    end
end
function DrawFireText(intensity, text, font, pos, size, color, glow, glow_color, shadow)
    local text_size = renderer.get_text_size(font, size, text)
    local fire_height = text_size.y * intensity
    for i = 1, text_size.x do
        local line_y = math.random(fire_height, text_size.y)
        local line_x = math.random(-4, 4)
        local line_col = math.random(0, 255)
        renderer.line(vec2_t.new(pos.x - 1 + i, pos.y + text_size.y), vec2_t.new(pos.x - 1 + i + line_x, pos.y + line_y), color_t.new(255, line_col, 0, 150))
    end
    if glow then
        DrawGlowingText(true, text, font, pos, size, color, glow_color)
    end
    if shadow then
        renderer.text(text, font, vec2_t.new(pos.x + 1, pos.y + 1), size, color_t.new(0, 0, 0, 255))
    end
    renderer.text(text, font, pos, size, color)
end
function DrawSnowingText(intensity, text, font, pos, size, color, color2)
    local color2 = color2 or color_t.new(255, 255, 255, 255)
    renderer.text(text, font, pos, size, color)
    local text_size = renderer.get_text_size(font, size, text)
    for i = 1, intensity do
        local line_y = math.random(0, text_size.y)
        local line_x = math.random(0, text_size.x)
        renderer.line(vec2_t.new(pos.x + line_x, pos.y + line_y), vec2_t.new(pos.x + line_x, pos.y + line_y + 1), color_t.new(color2.red * 255, color2.green * 255, color2.blue * 255, 255))
    end
end

function split(inputstr, sep)
    if sep == nil then
            sep = "%s"
    end
    local t={}
    for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
            table.insert(t, str)
    end
    return t
end


-- lib ended

notify.add(5,"welcome '".. client.get_username() .."' [build: debug] - version 1.0")

local actasym = renderer.setup_font("C:/windows/fonts/Acta Symbols W95 Arrows.ttf", 23, 12)
local veractasym = renderer.setup_font("C:/windows/fonts/verdana.ttf", 25, 12)

local menufunc = ui.add_combo_box("amnesia - ".. client.get_username(), "menufuncst", { "anti-aim", "visuals", "misc", "config" }, 0)

--menu visuals stuff
local watermarkenable = ui.add_check_box("enable full watermark", "fwater", false)
local infopanelenable = ui.add_check_box("enable info panel", "infpanel", false)
local colorinfopanel = ui.add_color_edit("choose color", "scope_color1", true, color_t.new(255, 255, 255, 255))
local getshotlogs = ui.add_check_box("enable logs", "shtlgs", false)
local logsstyle = ui.add_combo_box("logs style", "logsytle", { "none", "default", "modern" }, 0)
local snaplineenable = ui.add_check_box("enable heavy pistol snapline", "r8line", false)
local indicatorsenbale = ui.add_check_box("enable indicators", "indicatp", false)
local indiccombo = ui.add_combo_box("choose indicators", "indicatr", { "none", "#1", "#2", "#3" }, 0)
local colorindicat = ui.add_color_edit("choose color ", "colorind", true, color_t.new(255, 255, 255, 255))
local scale_thirdperson = ui.add_check_box("enable custom 3rd person", "scale_thirdperson", false)
local thirdperson_scale = ui.add_slider_int("3rd person radius", "thirdperson_scale", 30, 150, 0)
local arrowsenab = ui.add_check_box("enable arrows", "arrowsarrows", false)
local arrowscobmo = ui.add_combo_box("choose arrows", "arowss", { "none", "#1", "#2" }, 0)
local arrowscolor = ui.add_color_edit("choose color  ", "colorarrows", true, color_t.new(255, 255, 255, 255))
-- menu misc stuff

local trashtalk = ui.add_check_box("enable killsay", "killsay", false)
local jumpscout = ui.add_check_box("enable jumpscout", "jmpmoveap", false)
local pingspke = ui.add_check_box("enable ping on key", "eacmoveop", false)
local pskey = ui.add_key_bind("choose ping spike key", "ps_keybin", 0, 0)
local ps_slider = ui.add_slider_int("ping spike value", "ps_slid", 0, 200, 0)
local hideradr = ui.add_check_box("enable hide radar", "hdradr", false)
-- anti aim sutff

local aantibc = ui.add_check_box("enable avoid backstab", "antibcstb", false)

local fakeangles = ui.add_check_box("enable anti-aim", "antiaimenable", false)

local conditions = ui.add_combo_box("conditions", "condi", { "stand", "running", "crouch", "jumping", "air jump", "slow-walk" }, 0)


local pitch = ui.add_combo_box("pitch stand", "pitche", { "none", "down", "zero", "up" }, 0)
local baseyaw = ui.add_combo_box("base yaw stand", "baseyawe", { "none", "back", "left", "right" }, 0)
local yawjitl = ui.add_combo_box("yaw jitter stand", "yawkt", { "none", "center" }, 0)
local yawjiter = ui.add_slider_int("yaw jitter stand", "yawjit", -90, 90, 0)
local bodyyaw = ui.add_combo_box("body yaw stand", "yawdes", { "none", "static", "jitter" }, 0)
local fakelimit = ui.add_slider_int("fake limit stand", "faek", 0, 60, 0)
local attarget = ui.add_check_box("at target stand", "attarget", false)

local pitch11 = ui.add_combo_box("pitch move", "pitchemove", { "none", "down", "zero", "up" }, 0)
local baseyaw11 = ui.add_combo_box("base yaw move", "baseyawemove", { "none", "back", "left", "right" }, 0)
local yawjitl11 = ui.add_combo_box("yaw jitter mode move", "yawktmove", { "none", "center" }, 0)
local yawjiter11 = ui.add_slider_int("yaw jitter move", "yawjitmove", -90, 90, 0)
local bodyyaw11 = ui.add_combo_box("body yaw move", "yawdesmove", { "none", "static", "jitter" }, 0)
local fakelimit11 = ui.add_slider_int("fake limit move", "faekmove", 0, 60, 0)
local attarget11 = ui.add_check_box("at target move", "attargetmove", false)

local pitch22 = ui.add_combo_box("pitch crouch", "pitchecrouch", { "none", "down", "zero", "up" }, 0)
local baseyaw22 = ui.add_combo_box("base yaw crouch", "baseyawecrouch", { "none", "back", "left", "right" }, 0)
local yawjitl22 = ui.add_combo_box("yaw jitter mode crouch", "yawktcrouch", { "none", "center" }, 0)
local yawjiter22 = ui.add_slider_int("yaw jitter crouch", "yawjitcrouch", -90, 90, 0)
local bodyyaw22 = ui.add_combo_box("body yaw crouch", "yawdescrouch", { "none", "static", "jitter" }, 0)
local fakelimit22 = ui.add_slider_int("fake limit crouch", "faekcrouch", 0, 60, 0)
local attarget22 = ui.add_check_box("at target crouch", "attargetcrouch", false)

local pitch33 = ui.add_combo_box("pitch jump", "pitchejump", { "none", "down", "zero", "up" }, 0)
local baseyaw33 = ui.add_combo_box("base yaw jump", "baseyawejump", { "none", "back", "left", "right" }, 0)
local yawjitl33 = ui.add_combo_box("yaw jitter mode jump", "yawktjump", { "none", "center" }, 0)
local yawjiter33 = ui.add_slider_int("yaw jitter jump", "yawjitjump", -90, 90, 0)
local bodyyaw33 = ui.add_combo_box("body yaw jump", "yawdesjump", { "none", "static", "jitter" }, 0)
local fakelimit33 = ui.add_slider_int("fake limit jump", "faekjump", 0, 60, 0)
local attarget33 = ui.add_check_box("at target jump", "attargetjump", false)

local pitch55 = ui.add_combo_box("pitch air jump", "pitcheairjump", { "none", "down", "zero", "up" }, 0)
local baseyaw55 = ui.add_combo_box("base yaw air jump", "baseyaweairjump", { "none", "back", "left", "right" }, 0)
local yawjitl55 = ui.add_combo_box("yaw jitter mode air jump", "yawktairjump", { "none", "center" }, 0)
local yawjiter55 = ui.add_slider_int("yaw jitter air jump", "yawjitairjump", -90, 90, 0)
local bodyyaw55 = ui.add_combo_box("body yaw air jump", "yawdesairjump", { "none", "static", "jitter" }, 0)
local fakelimit55 = ui.add_slider_int("fake limit air jump", "faekairjump", 0, 60, 0)
local attarget55 = ui.add_check_box("at target air jump", "attargetairjump", false)

local pitch44 = ui.add_combo_box("pitch slow-walk", "pitcheslow-walk", { "none", "down", "zero", "up" }, 0)
local baseyaw44 = ui.add_combo_box("base yaw slow-walk", "baseyaweslow-walk", { "none", "back", "left", "right" }, 0)
local yawjitl44 = ui.add_combo_box("yaw jitter mode slow-walk", "yawktslow-walk", { "none", "center" }, 0)
local yawjiter44 = ui.add_slider_int("yaw jitter slow-walk", "yawjitslow-walk", -90, 90, 0)
local bodyyaw44 = ui.add_combo_box("body yaw slow-walk", "yawdesslow-walk", { "none", "static", "jitter" }, 0)
local fakelimit44 = ui.add_slider_int("fake limit slow-walk", "faekslow-walk", 0, 60, 0)
local attarget44 = ui.add_check_box("at target slow-walk", "attargetslow-walk", false)

local fakeangles11 = ui.get_check_box("antihit_antiaim_enable")
local baseyaw1 = ui.get_combo_box('antihit_antiaim_yaw')
local pitch1 =  ui.get_combo_box('antihit_antiaim_pitch')
local yawjiter1 = ui.get_slider_int("antihit_antiaim_yaw_jitter")
local bodyyaw1 = ui.get_combo_box("antihit_antiaim_desync_type")
local fakelimit1 = ui.get_slider_int("antihit_antiaim_desync_length")
local attarget1 = ui.get_check_box("antihit_antiaim_at_targets")

local slowWalkBint = ui.get_key_bind("antihit_accurate_walk_bind")
local m_bDucked = se.get_netvar("DT_BasePlayer", "m_bDucked")

local prevetnjit = ui.add_check_box("prevent sideways jitter", "jitterprev", false)
local enablemanual = ui.add_check_box("enable manual aa", "manualaa", false)
local manualleft = ui.add_key_bind("manual left", "lua_keybind_manualleft", 0, 0)
local manualright = ui.add_key_bind("manual right", "lua_keybind_manualright", 0, 0)
manualleft:set_type(3)
manualright:set_type(3)

local aaextrafunc = ui.add_multi_combo_box("aa - extra", "fudcnej2", { "freestand", "avoid all 0 desync degree", "fix pitch in shoot" }, { false, false, false })

local lua_fs_yaw = ui.add_key_bind("freestand key", "lua_fs_yaw", 0, 1)

local load = ui.add_check_box("load anti-aim config", "load", false)
local export = ui.add_check_box("export anti-aim config", "export", false)

local m_vecVelocity = {
    [0] = se.get_netvar("DT_BasePlayer", "m_vecVelocity[0]"),
    [1] = se.get_netvar("DT_BasePlayer", "m_vecVelocity[1]")
}

client.register_callback('create_move', function()
    local yawjiter2 = yawjiter:get_value()
    local fakelimit2 = fakelimit:get_value()
    local yawjiter3 = yawjiter11:get_value()
    local fakelimit3 = fakelimit11:get_value()
    local yawjiter4 = yawjiter22:get_value()
    local fakelimit4 = fakelimit22:get_value()
    local yawjiter5 = yawjiter33:get_value()
    local fakelimit5 = fakelimit33:get_value()
    local yawjiter6 = yawjiter44:get_value()
    local fakelimit6= fakelimit44:get_value()
    local yawjiter7 = yawjiter55:get_value()
    local fakelimit7 = fakelimit55:get_value()
    local penis = 1

    local localPlayer = entitylist.get_local_player()
    local m_hGroundEntity = localPlayer:get_prop_int(se.get_netvar("DT_BasePlayer", "m_hGroundEntity"))
    local duck = localPlayer:get_prop_int(se.get_netvar("DT_BasePlayer", "m_bDucked"))
    local velocity = math.sqrt(localPlayer:get_prop_float(m_vecVelocity[0]) ^ 2 + localPlayer:get_prop_float(m_vecVelocity[1]) ^ 2)

    if slowWalkBint:is_active() and duck == 0 then -- slowwalk

        if attarget44:get_value() then
            attarget1:set_value(true)
        else 
            attarget1:set_value(false)
        end
        if fakeangles:get_value() then
            fakeangles11:set_value(true)
        else 
            fakeangles11:set_value(false)
        end

        if pitch44:get_value() == 1 then
            pitch1:set_value(1)
        else if pitch44:get_value() == 2 then
            pitch1:set_value(2)
        else if pitch44:get_value() == 3 then
            pitch1:set_value(3)
        end
    end
    end

    
    if manualleft:is_active() and enablemanual:get_value() then
        baseyaw44:set_value(2)
    else if manualright:is_active() and enablemanual:get_value() then
        baseyaw44:set_value(3)
    else if enablemanual:get_value() then
        baseyaw44:set_value(1)
end
end
end
    
    if baseyaw44:get_value() == 1 then
        baseyaw1:set_value(1)
    else if baseyaw44:get_value() == 2 then
        baseyaw1:set_value(2)
    else if baseyaw44:get_value() == 3 then
        baseyaw1:set_value(3)
    end
    end
    end
    
    if yawjitl44:get_value() == 1 then
       yawjiter1:set_value(yawjiter6)
    else
        yawjiter1:set_value(0)
        
    end
    
    if penis == 1 then
        fakelimit1:set_value(fakelimit6)
     else
        fakelimit1:set_value(0)
         
     end
     
    
    if bodyyaw44:get_value() == 1 then
        bodyyaw1:set_value(0)
    
    else if bodyyaw44:get_value() == 2 then
        bodyyaw1:set_value(1)
    end
    end
    end

    if m_hGroundEntity == -1 and duck == 1 then -- airjumpjump

        if attarget55:get_value() then
            attarget1:set_value(true)
        else 
            attarget1:set_value(false)
        end
        if fakeangles:get_value() then
            fakeangles11:set_value(true)
        else 
            fakeangles11:set_value(false)
        end
        
        if pitch55:get_value() == 1 then
            pitch1:set_value(1)
        else if pitch55:get_value() == 2 then
            pitch1:set_value(2)
        else if pitch55:get_value() == 3 then
            pitch1:set_value(3)
        end
    end
    end
    
    if baseyaw55:get_value() == 1 then
        baseyaw1:set_value(1)
    else if baseyaw55:get_value() == 2 then
        baseyaw1:set_value(2)
    else if baseyaw55:get_value() == 3 then
        baseyaw1:set_value(3)
    end
    end
    end
    
    if yawjitl55:get_value() == 1 then
       yawjiter1:set_value(yawjiter7)
    else
        yawjiter1:set_value(0)
        
    end
    
    if penis == 1 then
        fakelimit1:set_value(fakelimit7)
     else
        fakelimit1:set_value(0)
         
     end
     
    
    if bodyyaw55:get_value() == 1 then
        bodyyaw1:set_value(0)
    
    else if bodyyaw55:get_value() == 2 then
        bodyyaw1:set_value(1)
    end
    end
    end


    if m_hGroundEntity == -1 and duck == 0 then -- jump

        if manualleft:is_active() and enablemanual:get_value() then
            baseyaw33:set_value(2)
        else if manualright:is_active() and enablemanual:get_value()then
            baseyaw33:set_value(3)
        else if enablemanual:get_value() then
            baseyaw33:set_value(1)
    end
    end
    end

        if attarget33:get_value() then
            attarget1:set_value(true)
        else 
            attarget1:set_value(false)
        end
        if fakeangles:get_value() then
            fakeangles11:set_value(true)
        else 
            fakeangles11:set_value(false)
        end
        
        if pitch33:get_value() == 1 then
            pitch1:set_value(1)
        else if pitch33:get_value() == 2 then
            pitch1:set_value(2)
        else if pitch33:get_value() == 3 then
            pitch1:set_value(3)
        end
    end
    end
    
    if baseyaw33:get_value() == 1 then
        baseyaw1:set_value(1)
    else if baseyaw33:get_value() == 2 then
        baseyaw1:set_value(2)
    else if baseyaw33:get_value() == 3 then
        baseyaw1:set_value(3)
    end
    end
    end
    
    if yawjitl33:get_value() == 1 then
       yawjiter1:set_value(yawjiter5)
    else
        yawjiter1:set_value(0)
        
    end
    
    if penis == 1 then
        fakelimit1:set_value(fakelimit5)
     else
        fakelimit1:set_value(0)
         
     end
     
    
    if bodyyaw33:get_value() == 1 then
        bodyyaw1:set_value(0)
    
    else if bodyyaw33:get_value() == 2 then
        bodyyaw1:set_value(1)
    end
    end
    end

    if m_hGroundEntity ~= -1 and duck == 1 then --crouch

        if manualleft:is_active() and enablemanual:get_value() then
            baseyaw22:set_value(2)
        else if manualright:is_active() and enablemanual:get_value() then
            baseyaw22:set_value(3)
        else if enablemanual:get_value() then
            baseyaw22:set_value(1)
    end
    end
    end

        if attarget22:get_value() then
            attarget1:set_value(true)
        else 
            attarget1:set_value(false)
        end
        if fakeangles:get_value() then
            fakeangles11:set_value(true)
        else 
            fakeangles11:set_value(false)
        end
        
        if pitch22:get_value() == 1 then
            pitch1:set_value(1)
        else if pitch22:get_value() == 2 then
            pitch1:set_value(2)
        else if pitch22:get_value() == 3 then
            pitch1:set_value(3)
        end
    end
    end
    
    if baseyaw22:get_value() == 1 then
        baseyaw1:set_value(1)
    else if baseyaw22:get_value() == 2 then
        baseyaw1:set_value(2)
    else if baseyaw22:get_value() == 3 then
        baseyaw1:set_value(3)
    end
    end
    end
    
    if yawjitl22:get_value() == 1 then
       yawjiter1:set_value(yawjiter4)
    else
        yawjiter1:set_value(0)
        
    end
    
    if penis == 1 then
        fakelimit1:set_value(fakelimit4)
     else
        fakelimit1:set_value(0)
         
     end
     
    
    if bodyyaw22:get_value() == 1 then
        bodyyaw1:set_value(0)
    
    else if bodyyaw22:get_value() == 2 then
        bodyyaw1:set_value(1)
    end
    end
    end
    
    if m_hGroundEntity ~= -1 and velocity < 5 and duck == 0 then --stand


    if attarget:get_value() then
        attarget1:set_value(true)
    else 
        attarget1:set_value(false)
    end
    if fakeangles:get_value() then
        fakeangles11:set_value(true)
    else 
        fakeangles11:set_value(false)
    end
    
    if pitch:get_value() == 1 then
        pitch1:set_value(1)
    else if pitch:get_value() == 2 then
        pitch1:set_value(2)
    else if pitch:get_value() == 3 then
        pitch1:set_value(3)
    end
end
end


if manualleft:is_active() and enablemanual:get_value() then
    baseyaw:set_value(2)
else if manualright:is_active() and enablemanual:get_value() then
    baseyaw:set_value(3)
else if enablemanual:get_value() then
    baseyaw:set_value(1)
end
end
end

if baseyaw:get_value() == 1 then
    baseyaw1:set_value(1)
else if baseyaw:get_value() == 2 then
    baseyaw1:set_value(2)
else if baseyaw:get_value() == 3 then
    baseyaw1:set_value(3)
end
end
end

if yawjitl:get_value() == 1 then
   yawjiter1:set_value(yawjiter2)
else
    yawjiter1:set_value(0)
    
end

if penis == 1 then
    fakelimit1:set_value(fakelimit2)
 else
    fakelimit1:set_value(0)
     
 end
 

if bodyyaw:get_value() == 1 then
    bodyyaw1:set_value(0)

else if bodyyaw:get_value() == 2 then
    bodyyaw1:set_value(1)
end
end
end

if m_hGroundEntity ~= -1 and velocity > 5 and duck == 0 then --move

    if manualleft:is_active() and enablemanual:get_value() then
        baseyaw11:set_value(2)
    else if manualright:is_active() and enablemanual:get_value()then
        baseyaw11:set_value(3)
    else if enablemanual:get_value() then
        baseyaw11:set_value(1)
end
end
end

    if not slowWalkBint:is_active() then
        if attarget11:get_value() then
            attarget1:set_value(true)
        else 
            attarget1:set_value(false)
        end
        if fakeangles:get_value() then
            fakeangles11:set_value(true)
        else 
            fakeangles11:set_value(false)
        end
        
        if pitch11:get_value() == 1 then
            pitch1:set_value(1)
        else if pitch11:get_value() == 2 then
            pitch1:set_value(2)
        else if pitch11:get_value() == 3 then
            pitch1:set_value(3)
        end
    end
    end
    
    if baseyaw11:get_value() == 1 then
        baseyaw1:set_value(1)
    else if baseyaw11:get_value() == 2 then
        baseyaw1:set_value(2)
    else if baseyaw11:get_value() == 3 then
        baseyaw1:set_value(3)
    end
    end
    end
    
    if yawjitl11:get_value() == 1 then
       yawjiter1:set_value(yawjiter3)
    else
        yawjiter1:set_value(0)
        
    end
    
    if penis == 1 then
        fakelimit1:set_value(fakelimit3)
     else
        fakelimit1:set_value(0)
         
     end
     
    
    if bodyyaw11:get_value() == 1 then
        bodyyaw1:set_value(0)
    
    else if bodyyaw11:get_value() == 2 then
        bodyyaw1:set_value(1)
    end
    end
    end
    end

end)



local verdana = renderer.setup_font("C:/windows/fonts/verdana.ttf", 13, 12)
local screensize = engine.get_screen_size()

--menu func
client.register_callback("paint", function()

        if conditions:get_value() == 0 and menufunc:get_value() == 0 and fakeangles:get_value() then
        pitch:set_visible(true)
        baseyaw:set_visible(true)
        yawjitl:set_visible(true)
        yawjiter:set_visible(true)
        bodyyaw:set_visible(true)
        fakelimit:set_visible(true)
        attarget:set_visible(true)
        else
            pitch:set_visible(false)
            baseyaw:set_visible(false)
            yawjitl:set_visible(false)
            yawjiter:set_visible(false)
            bodyyaw:set_visible(false)
            fakelimit:set_visible(false)
            attarget:set_visible(false)
        end
        if conditions:get_value() == 1 and menufunc:get_value() == 0 and fakeangles:get_value() then
            pitch11:set_visible(true)
            baseyaw11:set_visible(true)
            yawjitl11:set_visible(true)
            yawjiter11:set_visible(true)
            bodyyaw11:set_visible(true)
            fakelimit11:set_visible(true)
            attarget11:set_visible(true)
        else
            pitch11:set_visible(false)
            baseyaw11:set_visible(false)
            yawjitl11:set_visible(false)
            yawjiter11:set_visible(false)
            bodyyaw11:set_visible(false)
            fakelimit11:set_visible(false)
            attarget11:set_visible(false)
        end
        if conditions:get_value() == 2 and menufunc:get_value() == 0 and fakeangles:get_value() then
                pitch22:set_visible(true)
                baseyaw22:set_visible(true)
                yawjitl22:set_visible(true)
                yawjiter22:set_visible(true)
                bodyyaw22:set_visible(true)
                fakelimit22:set_visible(true)
                attarget22:set_visible(true)
        else
            pitch22:set_visible(false)
            baseyaw22:set_visible(false)
            yawjitl22:set_visible(false)
            yawjiter22:set_visible(false)
            bodyyaw22:set_visible(false)
            fakelimit22:set_visible(false)
            attarget22:set_visible(false)
        end
            if conditions:get_value() == 3 and menufunc:get_value() == 0 and fakeangles:get_value() then
                    pitch33:set_visible(true)
                    baseyaw33:set_visible(true)
                    yawjitl33:set_visible(true)
                    yawjiter33:set_visible(true)
                    bodyyaw33:set_visible(true)
                    fakelimit33:set_visible(true)
                    attarget33:set_visible(true)
            else
                pitch33:set_visible(false)
        baseyaw33:set_visible(false)
        yawjitl33:set_visible(false)
        yawjiter33:set_visible(false)
        bodyyaw33:set_visible(false)
        fakelimit33:set_visible(false)
        attarget33:set_visible(false)
            end
                if conditions:get_value() == 5 and menufunc:get_value() == 0 and fakeangles:get_value() then
                        pitch44:set_visible(true)
                        baseyaw44:set_visible(true)
                        yawjitl44:set_visible(true)
                        yawjiter44:set_visible(true)
                        bodyyaw44:set_visible(true)
                        fakelimit44:set_visible(true)
                        attarget44:set_visible(true)
                else
                    pitch44:set_visible(false)
                    baseyaw44:set_visible(false)
                    yawjitl44:set_visible(false)
                    yawjiter44:set_visible(false)
                    bodyyaw44:set_visible(false)
                    fakelimit44:set_visible(false)
                    attarget44:set_visible(false)
                end
    
            

    if pingspke:get_value() and menufunc:get_value() == 2 then
        pskey:set_visible(true)
        ps_slider:set_visible(true)
    else
        pskey:set_visible(false)
        ps_slider:set_visible(false)
    end


    if infopanelenable:get_value() and menufunc:get_value() == 1 then
        colorinfopanel:set_visible(true)
    else
        colorinfopanel:set_visible(false)
    end

    if indicatorsenbale:get_value() and menufunc:get_value() == 1 then
        indiccombo:set_visible(true)
        colorindicat:set_visible(true)
    else
        indiccombo:set_visible(false)
        colorindicat:set_visible(false)
    end
    
    if scale_thirdperson:get_value() and menufunc:get_value() == 1 then
        thirdperson_scale:set_visible(true)
    else
        thirdperson_scale:set_visible(false)
    end


    if getshotlogs:get_value() and menufunc:get_value() == 1 then
        logsstyle:set_visible(true)
    else
        logsstyle:set_visible(false)
    end
    --visuals
    if menufunc:get_value() == 1  then
        watermarkenable:set_visible(true)
        getshotlogs:set_visible(true)
        infopanelenable:set_visible(true)
        snaplineenable:set_visible(true)
        indicatorsenbale:set_visible(true)
        scale_thirdperson:set_visible(true)
    else
        watermarkenable:set_visible(false)
        getshotlogs:set_visible(false)
        infopanelenable:set_visible(false)
        snaplineenable:set_visible(false)
        indicatorsenbale:set_visible(false)
        scale_thirdperson:set_visible(false) 
    end
    if menufunc:get_value() == 2 then
        trashtalk:set_visible(true)
        jumpscout:set_visible(true)
        hideradr:set_visible(true)
        pingspke:set_visible(true)
    else
        trashtalk:set_visible(false)
        jumpscout:set_visible(false)
        pingspke:set_visible(false)
        hideradr:set_visible(false)
    end
    if menufunc:get_value() == 0 then
        enablemanual:set_visible(true)
        aantibc:set_visible(true)
        fakeangles:set_visible(true)
    else
        enablemanual:set_visible(false)
        aantibc:set_visible(false)
        fakeangles:set_visible(false)
    end
    if menufunc:get_value() == 0 and fakeangles:get_value() then
        conditions:set_visible(true)
        manualleft:set_visible(true)
        manualright:set_visible(true)
    else
        conditions:set_visible(false)
        manualleft:set_visible(false)
        manualright:set_visible(false)
    end
    if menufunc:get_value() == 0 and enablemanual:get_value() then
        manualleft:set_visible(true)
        manualright:set_visible(true)
    else
        manualleft:set_visible(false)
        manualright:set_visible(false)
    end
end)

client.register_callback("paint", function()

    if conditions:get_value() == 4 and menufunc:get_value() == 0 and fakeangles:get_value() then
        pitch55:set_visible(true)
        baseyaw55:set_visible(true)
        yawjitl55:set_visible(true)
        yawjiter55:set_visible(true)
        bodyyaw55:set_visible(true)
        fakelimit55:set_visible(true)
        attarget55:set_visible(true)
    else
        pitch55:set_visible(false)
        baseyaw55:set_visible(false)
        yawjitl55:set_visible(false)
        yawjiter55:set_visible(false)
        bodyyaw55:set_visible(false)
        fakelimit55:set_visible(false)
        attarget55:set_visible(false)
    end

    if menufunc:get_value() == 0 and aaextrafunc:get_value(0) then
        lua_fs_yaw:set_visible(true)
    else
        lua_fs_yaw:set_visible(false)
    end

    if menufunc:get_value() == 0 then
        aaextrafunc:set_visible(true)
    else
        aaextrafunc:set_visible(false)
    end

if menufunc:get_value() == 0 and fakeangles:get_value() then
    prevetnjit:set_visible(true)
else
    prevetnjit:set_visible(false)
end
if menufunc:get_value() == 3 then
    load:set_visible(true)
    export:set_visible(true)
else
    load:set_visible(false)
    export:set_visible(false)
end
if menufunc:get_value() == 2 then
    dfenbale:set_visible(true)
else
    dfenbale:set_visible(false)
end
if menufunc:get_value() == 1 then
    arrowsenab:set_visible(true)
else
    arrowsenab:set_visible(false)
end

if menufunc:get_value() == 1 and arrowsenab:get_value() then
    arrowscobmo:set_visible(true)
    arrowscolor:set_visible(true)
else
    arrowscolor:set_visible(false)
    arrowscobmo:set_visible(false)
end

if menufunc:get_value() == 2 and dfenbale:get_value() then
    idkhowtodie1:set_visible(true)
else
    idkhowtodie1:set_visible(false)
end

end)


local elements = {
    aantibc,
    fakeangles,
    pitch,
    baseyaw,
    yawjiter,
    bodyyaw,
    fakelimit,
    attarget,
    pitch11,
    baseyaw11,
    yawjitl11,
    yawjiter11,
    bodyyaw11,
    fakelimit11,
    attarget11,
    pitch22,
    baseyaw22,
    yawjitl22,
    yawjiter22,
    bodyyaw22,
    fakelimit22,
    attarget22,
    pitch33,
    baseyaw33,
    yawjitl33,
    yawjiter33,
    bodyyaw33,
    fakelimit33,
    fakelimit33,
    attarget33,
    pitch55,
    baseyaw55,
    yawjitl55,
    yawjiter55,
    bodyyaw55,
    fakelimit55,
    attarget55,
    pitch44,
    baseyaw44,
    yawjitl44,
    yawjiter44,
    bodyyaw44,
    fakelimit44,
    attarget44,
    prevetnjit,
    enablemanual,
}

local systems = {
    config = {
        load = function()
            local import = clipboard.get()
            local config = split(import, ",")
            for i=1,#elements do
                elements[i]:set_value(config[i])
            end
        end,
        export = function()
            local config = ""
            for i=1,#elements do
                if i == #elements then
                    config = config .. tostring(elements[i]:get_value())
                else
                    config = config .. tostring(elements[i]:get_value()) .. ","
                end
            end
            clipboard.set(config)
        end
    },
}

client.register_callback('paint', function()
    if load:get_value() then
        systems.config.load()       
        client.notify("1")
        load:set_value(false)
     end

     if export:get_value() then
        systems.config.export()
        client.notify("0")
        export:set_value(false)
     end
end)



--visuals func

--watermark
local function watermark()
    if watermarkenable:get_value() and engine.is_connected() then
    local nameclient = client.get_username()
    local ping = se.get_latency()
    local text = "amnesia.lua [debug] / " .. nameclient .. " " .. ping .. " ms " .. os.date("%H:%M").. " pm"

    local w = renderer.get_text_size(verdana, 13, text).x + 10
    local x = screensize.x - w - 10

    renderer.rect_filled(vec2_t.new(x -2, 8), vec2_t.new(w + x +2, 32), color_t.new(70,70, 70, 255))
    renderer.rect_filled(vec2_t.new(x -1, 9), vec2_t.new(w + x +1, 31), color_t.new(0,0, 0, 255))
    renderer.rect_filled(vec2_t.new(x, 10), vec2_t.new(w + x, 30), color_t.new(27,27, 27, 255))

    --cosmetic stuff
    renderer.filled_polygon({vec2_t.new(x + 8, 11), vec2_t.new(x +63, 30), vec2_t.new(x + 96, 11)}, color_t.new(70,70, 70, 120))
    renderer.filled_polygon({vec2_t.new(x + w - 126, 30), vec2_t.new(x +w -97, 10), vec2_t.new(x + w - 40, 11)}, color_t.new(70,70, 70, 120))
    --
    
    renderer.text(text, verdana, vec2_t.new(x + 4, 13), 0, color_t.new(255, 255, 255, 255))
    end
end

client.register_callback("paint", watermark)

--info panel

local function infopanel()
    if infopanelenable:get_value() and engine.is_connected() then
        local colinfopanel = colorinfopanel:get_value()

        renderer.text(">/ amnesia system - ".. client.get_username() .." - [debug] /<", verdana, vec2_t.new(screensize.x - screensize.x + 10 +1, screensize.y/ 2 - 39), 0, color_t.new(0,0, 0, 255))
        DrawEnchantedText(2, ">/ amnesia system - ".. client.get_username() .." - [debug] /<", verdana, vec2_t.new(screensize.x - screensize.x + 10, screensize.y/ 2 - 40), 13, color_t.new(0,0, 0, 255), colinfopanel,colinfopanel,colinfopanel,colinfopanel)
    end
end

client.register_callback("paint", infopanel)

-- shot logs

ffi.cdef[[
    struct c_color { unsigned char clr[4];};
]]

local log = {}

local console_color = ffi.new('struct c_color')
log.console_print = function(color, text)
    local engine_cvar = ffi.cast('void***', se.create_interface('vstdlib.dll', 'VEngineCvar007'))
    local console_print = ffi.cast('void(__cdecl*)(void*, const struct c_color&, const char*, ...)', engine_cvar[0][25])
    console_color.clr = {[0] = color.red * 255, [1] = color.green * 255, [2] = color.blue * 255, [3] = color.alpha * 255}
    console_print(engine_cvar, console_color, text)
end

local logs = {}

log.add = function(text, color)
    log.console_print(color, '[amnesia] ' .. text .. '\n')
    table.insert(logs, 1, { text = text, color = color, alpha = 0, time = globalvars.get_current_time() + 4 })
end

math.lerp = function(a, b, time)
    return a + (b - a) * time
end

local font = renderer.setup_font('C:/windows/fonts/lucon.ttf', 10, 400)

log.render = function()

    local offset = 0
    local local_player = entitylist.get_local_player()
    for i, v in pairs(logs) do
        if v.time > globalvars.get_current_time() and i <= 11 and engine.is_connected() then
            v.alpha = math.lerp(v.alpha, 255, 0.13)
        else
            v.alpha = math.lerp(v.alpha, 0, 0.13)
            if v.alpha < 1 then
                table.remove(logs, i)
            end
        end

        if logsstyle:get_value() == 1 then
        renderer.text("[amnesia] >> ".. v.text, font, vec2_t.new(5 + 1, 3+ 1 + offset), 10, color_t.new(0, 0, 0, v.alpha))
        renderer.text("[amnesia] >> ".. v.text, font, vec2_t.new(5, 3 + offset), 10, color_t.new(v.color.red * 255, v.color.green * 255, v.color.blue * 255, v.alpha))
        end
        offset = offset + 16 * (v.alpha / 255)
    end
end

local hitgroups = {
    [0] = 'generic',
    [1] = 'head',
    [2] = 'chest',
    [3] = 'stomach',
    [4] = 'left arm',
    [5] = 'right arm',
    [6] = 'left leg',
    [7] = 'right leg',
    [8] = 'neck',
    [10] = 'gear'
}

local hitboxes = {
    [0] = 'head',
    [4] = 'chest',
    [5] = 'chest',
    [6] = 'chest',
    [2] = 'stomach',
    [3] = 'stomach',
    [13] = 'left arm',
    [15] = 'left arm',
    [16] = 'left arm',
    [14] = 'right arm',
    [17] = 'right arm',
    [18] = 'right arm',
    [7] = 'left leg',
    [9] = 'left leg',
    [11] = 'left leg',
    [8] = 'right leg',
    [10] = 'right leg', 
    [12] = 'right leg'
}


local weapon_to_verb = { hegrenade = 'Naded', inferno = 'Burned', knife = 'Knifed', taser = 'Zeused' }

log.player_hurt = function(event)
    local local_player = engine.get_player_info(entitylist.get_local_player():get_index()).name
    local target = engine.get_player_info(engine.get_player_for_user_id(event:get_int('userid', 0))).name
    local attacker = engine.get_player_info(engine.get_player_for_user_id(event:get_int('attacker', 0))).name
    local damage = event:get_int('dmg_health', 0)
    local hitgroup = hitgroups[event:get_int('hitgroup', 0)]
    local remaining = event:get_int('health', 0)
    local weapon = event:get_string('weapon', '')
    local verb = weapon_to_verb[weapon] ~= nil and weapon_to_verb[weapon] or ''

    if attacker == local_player and target ~= local_player and target ~= nil then
    
        if hitgroup == 'generic' and logsstyle:get_value() == 2 then
            log.add(("Registered damage in %s's %s for %s damage (safety: ".. math.random(0,1) .." | flags: ".. math.random(0,1) .."".. math.random(0,1) .."".. math.random(0,1) ..") [%s remaining]"):format(target, hitgroup, damage, remaining), color_t.new(255, 255, 255, 255))
        elseif logsstyle:get_value() == 2 then
            notify.add(5,"Shot in ".. target .."'s ".. hitgroup .." for ".. damage .." damage [".. remaining .." remaining]")
        end

        if hitgroup == 'generic' and logsstyle:get_value() == 1 then
            log.add(("Registered damage in %s's %s for %s damage (safety: ".. math.random(0,1) .." | flags: ".. math.random(0,1) .."".. math.random(0,1) .."".. math.random(0,1) ..") [%s remaining]"):format(target, hitgroup, damage, remaining), color_t.new(255, 255, 255, 255))
        elseif logsstyle:get_value() == 1 then
            log.add(("Registered shot in %s's %s for %s damage (safety: ".. math.random(0,1) .." | flags: ".. math.random(0,1) .."".. math.random(0,1) .."".. math.random(0,1) ..") [%s remaining]"):format(target, hitgroup, damage, remaining), color_t.new(255, 255, 255, 255))
        end
        end
    
end

    log.shot_fired = function(shot_info)
        local target = engine.get_player_info(shot_info.target_index).name
        local hitbox = hitboxes[shot_info.hitbox]
        local reason = shot_info.result == 'unk' and 'unknown' or shot_info.result
        local manual = shot_info.manual
        local hitch = shot_info.hitchance
        local dmge = shot_info.client_damage
        local sfty = shot_info.safe_point
        local hstry = shot_info.backtrack
        if logsstyle:get_value() == 1 and reason ~= 'hit' and not manual then
            log.add(("Missed shot at %s's %s due to %s ( dmg: %s | safety: %s | history(?): %s )"):format(target, hitbox, reason, dmge, sfty, hstry), color_t.new(255, 255, 255, 255))
        else if logsstyle:get_value() == 2 and reason ~= 'hit' and not manual then
            notify.add(5,"Missed in ".. target .."'s ".. hitbox .." due to ".. reason .."")
        end
        end
    end

    log.item_purchase = function(event)
        local player = engine.get_player_info(engine.get_player_for_user_id(event:get_int('userid', 0))).name
        local weapon = event:get_string('weapon', '')
        local item = weapon:find('weapon_') and weapon:gsub('weapon_', '') or weapon:find('item_') and weapon:gsub('item_', '')
        
        if logsstyle:get_value() == 1 and weapon ~= 'weapon_unknown' then
            log.add(('%s buyed %s'):format(player, item), color_t.new(255, 255, 255, 255))
        end
    end
    

client.register_callback('paint', log.render)
client.register_callback('player_hurt', log.player_hurt)
client.register_callback('shot_fired', log.shot_fired)
client.register_callback('item_purchase', log.item_purchase)

-- snapline
local function infinty()
    local hwid = client.get_username()
        local is_whitelisted = false

        for i, username in ipairs(whitelist) do
            if username == hwid then
                is_whitelisted = true
                break
            end
        end
        if is_whitelisted then
        else
            client.notify("- didnt connected to server (you not in database)")
            client.unload_script(SCRIPT_NAME)
        end
    end

client.register_callback("paint", infinty)

function engine.get_weapon_name()
    local weapon = {
        [589825] = "Deagle",
        [1] = "Deagle",-- Desert Eagle
        [589888] = "Revolver",
        [262208] = "Revolver",
        [64] = "Revolver",-- Revolver
    }
    return weapon[entitylist.get_entity_from_handle(entitylist.get_local_player():get_prop_int(0x2F08)):get_prop_int(0x2FBA)]
end

local m_vecOrigin = se.get_netvar("DT_BaseEntity", "m_vecOrigin")
function snapline()
    if snaplineenable:get_value() then
    local mem4ik = engine.get_weapon_name()
    local is_thirdperson = ui.get_check_box("visuals_thirdperson"):get_value() and ui.get_key_bind("visuals_thirdperson_bind"):is_active()
    local local_player = entitylist.get_local_player()
    local local_player_body_pos = local_player:get_player_hitbox_pos(3)
    local w2s_lp = se.world_to_screen(local_player_body_pos)
    if mem4ik == "Deagle" then
    local enemy_player = entitylist.get_players(0)
    for i = 1, #enemy_player do
        local enemy_player_body_pos = enemy_player[i]:get_player_hitbox_pos(3)
        local w2s_ep = se.world_to_screen(enemy_player_body_pos)
        if w2s_ep.x ~= 0 and w2s_ep.y ~= 0 and enemy_player[i]:is_alive() and not enemy_player[i]:is_dormant() then
            if is_thirdperson then
                renderer.line(w2s_lp, w2s_ep, color_t.new(255, 30, 30, 255))
            else
                renderer.line(vec2_t.new(screensize.x/2, screensize.y), w2s_ep, color_t.new(255, 30, 30, 255))
            end
        end
    end
    end
    end
end

client.register_callback("paint", snapline)

function snapline1()
    if snaplineenable:get_value() then
    local mem4ik = engine.get_weapon_name()
    local is_thirdperson = ui.get_check_box("visuals_thirdperson"):get_value() and ui.get_key_bind("visuals_thirdperson_bind"):is_active()
    local local_player = entitylist.get_local_player()
    local local_player_body_pos = local_player:get_player_hitbox_pos(3)
    local w2s_lp = se.world_to_screen(local_player_body_pos)
    if mem4ik == "Revolver" then
    local enemy_player = entitylist.get_players(0)
    for i = 1, #enemy_player do
        local enemy_player_body_pos = enemy_player[i]:get_player_hitbox_pos(3)
        local w2s_ep = se.world_to_screen(enemy_player_body_pos)
        if w2s_ep.x ~= 0 and w2s_ep.y ~= 0 and enemy_player[i]:is_alive() and not enemy_player[i]:is_dormant() then
            if is_thirdperson then
                renderer.line(w2s_lp, w2s_ep, color_t.new(255, 30, 30, 255))
            else
                renderer.line(vec2_t.new(screensize.x/2, screensize.y), w2s_ep, color_t.new(255, 30, 30, 255))
            end
        end
    end
    end
    end
end

client.register_callback("paint", snapline1)

--indicators

local smallest = renderer.setup_font("C:/windows/fonts/smallest_pixel-7.ttf", 10, 8)


local slowWalkBint = ui.get_key_bind("antihit_accurate_walk_bind")
local m_bDucked = se.get_netvar("DT_BasePlayer", "m_bDucked")

local m_vecVelocity = {
    [0] = se.get_netvar("DT_BasePlayer", "m_vecVelocity[0]"),
    [1] = se.get_netvar("DT_BasePlayer", "m_vecVelocity[1]")
}

function get_cond()
    localPlayer = entitylist.get_local_player()
    m_hGroundEntity = localPlayer:get_prop_int(se.get_netvar("DT_BasePlayer", "m_hGroundEntity"))
    duck = localPlayer:get_prop_int(se.get_netvar("DT_BasePlayer", "m_bDucked"))
    velocity = math.sqrt(localPlayer:get_prop_float(m_vecVelocity[0]) ^ 2 + localPlayer:get_prop_float(m_vecVelocity[1]) ^ 2)
    if m_hGroundEntity == -1 then
        return "jumping"
    elseif m_hGroundEntity ~= -1 and duck == 1 then
        return "crouch"
    elseif m_hGroundEntity ~= -1 and velocity > 5 then
        if not slowWalkBint:is_active() then
            return "running"
        else
            return "slowwalking"
        end
    elseif m_hGroundEntity ~= -1 and velocity < 5 then
        return "stand"
    end
end

local yadd = 0
local m = 0
local offset_scope = 0
local m_bIsScoped = se.get_netvar("DT_CSPlayer", "m_bIsScoped")
local local_player = entitylist:get_local_player()
local anim_num = 0

local lerp = function(a, b, t)
    return a + (b - a) * t
end

local m_bIsScoped = se.get_netvar("DT_CSPlayer", "m_bIsScoped")
local m_iHealth = se.get_netvar("DT_BasePlayer", "m_iHealth")
local FRAME_RENDER_START = 5

local m_iScope = false

-- Return scope
client.register_callback("frame_stage_notify", function(stage)
	if stage ~= FRAME_RENDER_START then 
		return 
	end
	
	local local_player = entitylist:get_local_player()

	-- Only for bool check
	-- To draw something on paint
	if local_player:get_prop_bool(se.get_netvar("DT_CSPlayer", "m_bIsScoped" )) then
		m_iScope = true		
	else
		m_iScope = false	
	end
	
end)

local netvars = {
    m_flvelocitymodifier = se.get_netvar("DT_CSPlayer", "m_flVelocityModifier")
}

local function indicators()
 if indicatorsenbale:get_value() and indiccombo:get_value() == 1 and engine.is_connected() then
    local colorind = colorindicat:get_value()
    local dt = ui.get_key_bind("rage_doubletap_bind")
    local hs = ui.get_key_bind("rage_hide_shots_bind")
    local duck = ui.get_key_bind("antihit_fakeduck_bind")
    local dmg  = ui.get_key_bind("rage_min_damage_override_bind")
    local wtext = renderer.get_text_size(smallest, 10, "AMNESIAYAW").x /2
    local wcond = renderer.get_text_size(smallest, 10, get_cond()).x /2
    local wdt = renderer.get_text_size(smallest, 10, "dt").x /2

	if m_iScope == true then
		m = 1
	else
		m = 0
	end
	

    local offsett = 40

        anim_num = lerp(anim_num, m, 15 * globalvars.get_frame_time())

    offsett = 1 and offsett * anim_num or offsett
    local yadd = 0
    DrawGlowingText(true, "AMNESIAYAW", smallest, vec2_t.new(screensize.x/2 -wtext+1 + offsett, screensize.y /2 + 25), 10, color_t.new(255, 255, 255, 255), colorind,colorind,colorind,colorind)
    DrawEnchantedText(2, "AMNESIAYAW", smallest, vec2_t.new(screensize.x/2 -wtext+1 + offsett, screensize.y /2 + 25), 10, color_t.new(255, 255, 255, 255), colorind,colorind,colorind,colorind)
    DrawGlowingText(true, get_cond(), smallest, vec2_t.new(screensize.x/2 -wcond+1 + offsett, screensize.y /2 + 34), 10, color_t.new(255, 255, 255, 255), color_t.new(0, 0, 0, 255))


    if dmg:is_active() then
        DrawGlowingText(true, "damage", smallest, vec2_t.new(screensize.x/2 - renderer.get_text_size(smallest, 10, "damage").x /2 +1+ offsett, screensize.y /2 + 43 +yadd), 10, color_t.new(255, 255, 255, 255), color_t.new(0, 0, 0, 255))
        yadd = yadd + 9
   end 
    if dt:is_active() then
        DrawGlowingText(true, "dt", smallest, vec2_t.new(screensize.x/2 -wdt +1+ offsett, screensize.y /2 + 43 +yadd), 10, color_t.new(255, 255, 255, 255), color_t.new(0, 0, 0, 255))
        yadd = yadd + 9
    end
    if hs:is_active() then
        DrawGlowingText(true, "HIDE", smallest, vec2_t.new(screensize.x/2 - renderer.get_text_size(smallest, 10, "hide").x /2 +1+ offsett, screensize.y /2 + 43 +yadd), 10, color_t.new(255, 255, 255, 255), color_t.new(0, 0, 0, 255))
        yadd = yadd + 9
        end
    if duck:is_active() then
            DrawGlowingText(true, "DUCK", smallest, vec2_t.new(screensize.x/2 - renderer.get_text_size(smallest, 10, "DUCK").x /2 +1+ offsett, screensize.y /2 + 43 +yadd), 10, color_t.new(255, 255, 255, 255), color_t.new(0, 0, 0, 255))
            yadd = yadd + 9
    end
   if pskey:is_active() then
    DrawGlowingText(true, "PING", smallest, vec2_t.new(screensize.x/2 - renderer.get_text_size(smallest, 10, "PING").x /2 +1+ offsett, screensize.y /2 + 43 +yadd), 10, color_t.new(255, 255, 255, 255), color_t.new(0, 0, 0, 255))
    yadd = yadd + 9
end 
end
end

client.register_callback("paint", indicators)



local verdana_small = renderer.setup_font("C:/windows/fonts/verdana.ttf", 12, 14)
local FixedNumber = function(number, value) return string.format("%g",string.format("%."..value.."f",number)) end

local function indicators1()
    if indicatorsenbale:get_value() and indiccombo:get_value() == 2 and engine.is_connected() then
    local colorind = colorindicat:get_value()
    local dt = ui.get_key_bind("rage_doubletap_bind")
    local hs = ui.get_key_bind("rage_hide_shots_bind")
    local duck = ui.get_key_bind("antihit_fakeduck_bind")
	local local_player = entitylist.get_local_player()
    local velmodifier = local_player:get_prop_float(netvars.m_flvelocitymodifier)
    local dmg  = ui.get_key_bind("rage_min_damage_override_bind")
    local wtext = renderer.get_text_size(verdana_small, 12, "amnesia").x /2
    local wdt = renderer.get_text_size(verdana_small, 12, "dt").x /2
    local alpha = math.floor(math.sin(math.abs((math.pi * -1) + (globalvars.get_real_time() * 1.5) % (math.pi * 2))) * 255);
    local fixedmodifier = FixedNumber(velmodifier * 100, 0)
    
	if m_iScope == true then
		m = 1
	else
		m = 0
	end
	

    local offsett = 35

        anim_num = lerp(anim_num, m, 15 * globalvars.get_frame_time())

    offsett = 1 and offsett * anim_num or offsett
    
    local yadd = 0
    DrawShadowedText(2, "amnesia", verdana_small, vec2_t.new(screensize.x/2 -wtext+1 + offsett, screensize.y /2 + 25), 12, colorind,colorind,colorind,colorind)
    renderer.text("debug", verdana_small, vec2_t.new(screensize.x/2 - renderer.get_text_size(verdana_small, 12, "debug").x /2+2 + offsett, screensize.y /2 + 37),12, color_t.new(0, 0, 0, alpha))
    renderer.text("debug", verdana_small, vec2_t.new(screensize.x/2 - renderer.get_text_size(verdana_small, 12, "debug").x /2+1 + offsett, screensize.y /2 + 36),12, color_t.new(255, 255, 255, alpha))

    if dt:is_active() then
        DrawShadowedText(2, "dt", verdana_small, vec2_t.new(screensize.x/2 -wdt + 1+ offsett, screensize.y /2 + 47 +yadd), 12, color_t.new(255, 255, 255, 255))
        yadd = yadd + 11
    end
    if hs:is_active() then
        DrawShadowedText(2, "hide", verdana_small, vec2_t.new(screensize.x/2 - renderer.get_text_size(verdana_small, 12, "hide").x /2 + 2+ offsett, screensize.y /2 + 47 +yadd), 12, color_t.new(255, 255, 255, 255))
        yadd = yadd + 11
        end
    if duck:is_active() then
        DrawShadowedText(2, "duck", verdana_small, vec2_t.new(screensize.x/2 - renderer.get_text_size(verdana_small, 12, "duck").x /2 +2+ offsett, screensize.y /2 + 47 +yadd), 12, color_t.new(255, 255, 255, 255))
            yadd = yadd + 11
    end
   if velmodifier ~= 1 then
       DrawShadowedText(2, "slow: ".. fixedmodifier .. "%", verdana_small, vec2_t.new(screensize.x/2 - renderer.get_text_size(verdana_small, 12, "slow: ".. fixedmodifier .. "%").x /2 +1+ offsett, screensize.y /2 + 47 +yadd), 12, color_t.new(255, 255, 255, 255))
        yadd = yadd + 11
  end 
    if dmg:is_active() then
        DrawShadowedText(2, "dmg", verdana_small, vec2_t.new(screensize.x/2 - renderer.get_text_size(verdana_small, 12, "dmg").x /2 +1+ offsett, screensize.y /2 + 47 +yadd), 12, color_t.new(255, 255, 255, 255))
        yadd = yadd + 11
   end 
   if ui.get_key_bind("rage_hitscan_override_bind"):is_active() then
    DrawShadowedText(2, "baim", verdana_small, vec2_t.new(screensize.x/2 - renderer.get_text_size(verdana_small, 12, "baim").x /2 +1+ offsett, screensize.y /2 + 47 +yadd), 12, color_t.new(255, 255, 255, 255))
    yadd = yadd + 11
   end
   if pskey:is_active() then
    DrawShadowedText(2, "ping", verdana_small, vec2_t.new(screensize.x/2 - renderer.get_text_size(verdana_small, 12, "ping").x /2 +2+ offsett, screensize.y /2 + 47 +yadd), 12, color_t.new(255, 255, 255, 255))
    yadd = yadd + 11
   end
end 
end

client.register_callback("paint", indicators1)

local function indicators()
    if indicatorsenbale:get_value() and indiccombo:get_value() == 3 and engine.is_connected() then
    local colorind = colorindicat:get_value()
    local dt = ui.get_key_bind("rage_doubletap_bind")
    local hs = ui.get_key_bind("rage_hide_shots_bind")
    local duck = ui.get_key_bind("antihit_fakeduck_bind")
    local dmg  = ui.get_key_bind("rage_min_damage_override_bind")
    local wtext = renderer.get_text_size(verdana_small, 12, "amnesia").x /2
    local wcond = renderer.get_text_size(smallest, 10, get_cond()).x /2
    local wdt = renderer.get_text_size(smallest, 10, "dt").x /2
    local state = ""

	if m_iScope == true then
		m = 1
	else
		m = 0
	end
	

    local offsett = 32

        anim_num = lerp(anim_num, m, 15 * globalvars.get_frame_time())

    offsett = 1 and offsett * anim_num or offsett
    

    if dt:is_active() or hs:is_active() then
        state = get_cond()
    else
        state = "FAKELAG"
    end

    local yadd = 0
    DrawGlowingText(true, "amnesia", verdana_small, vec2_t.new(screensize.x/2 -wtext+1 + offsett, screensize.y /2 + 25), 12, color_t.new(255, 255, 255, 255), colorind,colorind,colorind ,colorind)
    DrawEnchantedText(2, "amnesia", verdana_small, vec2_t.new(screensize.x/2 -wtext+1 + offsett, screensize.y /2 + 25), 12, color_t.new(255, 255, 255, 255), colorind,colorind,colorind,colorind)
    DrawGlowingText(true, state, smallest, vec2_t.new(screensize.x/2 -renderer.get_text_size(smallest, 10, state).x /2+1 + offsett, screensize.y /2 + 35), 10, colorind, color_t.new(0, 0, 0, 255))

    if dt:is_active() then
        DrawGlowingText(true, "DT", smallest, vec2_t.new(screensize.x/2 +1-wdt + offsett, screensize.y /2 + 43 +yadd), 10, color_t.new(75, 255, 125, 255), color_t.new(0, 0, 0, 255))
        yadd = yadd + 8
    end
    if hs:is_active() then
        DrawGlowingText(true, "HIDE", smallest, vec2_t.new(screensize.x/2 - renderer.get_text_size(smallest, 10, "hide").x /2 + 1 + offsett, screensize.y /2 + 43 +yadd), 10, color_t.new(155, 172, 135, 255), color_t.new(0, 0, 0, 255))
        yadd = yadd + 8
        end
    if duck:is_active() then
            DrawGlowingText(true, "DUCK", smallest, vec2_t.new(screensize.x/2 - renderer.get_text_size(smallest, 10, "duck").x /2 + 1 + offsett, screensize.y /2 + 43 +yadd), 10, color_t.new(255, 255, 255, 255), color_t.new(0, 0, 0, 255))
            yadd = yadd + 8
    end
    if pskey:is_active() then
        DrawGlowingText(true, "PING", smallest, vec2_t.new(screensize.x/2 - renderer.get_text_size(smallest, 10, "ping").x /2 + 1 + offsett, screensize.y /2 + 43 +yadd), 10, color_t.new(255, 255, 255, 255), color_t.new(0, 0, 0, 255))
        yadd = yadd + 8
    end 
    if dmg:is_active() then
        DrawGlowingText(true, "DMG", smallest, vec2_t.new(screensize.x/2 - renderer.get_text_size(smallest, 10, "dmg").x /2 + 0.8 + offsett, screensize.y /2 + 43 +yadd), 10, color_t.new(220, 155, 175, 255), color_t.new(0, 0, 0, 255))
        yadd = yadd + 8
   end 
end
end

client.register_callback("paint", indicators)


-- thirdpersn

once_thirdperson = false

client.register_callback("create_move", function()
if scale_thirdperson:get_value() then se.get_convar("cam_idealdist"):set_int(scale_thirdperson:get_value() and thirdperson_scale:get_value() or 120) end
if scale_thirdperson:get_value() and not once_thirdperson then once_thirdperson = not once_thirdperson end
if not scale_thirdperson:get_value() and once_thirdperson then once_thirdperson = not once_thirdperson; se.get_convar("cam_idealdist"):set_int(120) end
end) client.register_callback("unload", function() if scale_thirdperson:get_value() then se.get_convar("cam_idealdist"):set_int(120) end end)

-- misc stuff

local messages = {
    'amnesia.lua - <3 ',
    'ナイトコア.lua AMNESIA L.A ナイトコア？？？ HACK NIXWARE ナイトコア？？ 100% QUALITY ナイトコア.lua AMNESIA。JAPAN ナイトコア',
    'KILLED AGAIN？？ AMNESIA.lua',
    'fooled ^^',
    'настолько ты хуевый что он никсвара умер',
    'че в хуй я юзер амнесия.луа ебанутый',
"че сынуля опять на коленях?",
"zxc, f9 - ????",
"ебу твою мать",
"кто в тя харкал изи",
"чесхуя отцу гею скажешь",
"дадада попизди что ты не впенен амнесия луа",
'Дотер? С amnesia играешь?))',
"amnesia kangal preset 2023 OMG coder inovation",
"amnesia ^__^ ;D :3",
"powerful by amnesia",
"discord.gg/GAZZmqhka7 - best nixware lua"
}
    



client.register_callback("player_death", function(event)
    if trashtalk:get_value() then
    local attacker_index = engine.get_player_for_user_id(event:get_int("attacker",0))
    local died_index = engine.get_player_for_user_id(event:get_int("userid",1))
    local me = engine.get_local_player()
    
    math.randomseed(os.clock()*100000000)

        if attacker_index == me and died_index ~= me then            
            
            engine.execute_client_cmd("say " .. tostring(messages[math.random(1, #messages)]))
      
        end
    end
end)


local function jumpscot()
    if jumpscout:get_value() and engine.is_connected() then
    if engine.is_connected() then

        local localPlayer = entitylist.get_local_player()
            
        m_vecVelocity = {
            [0] = se.get_netvar("DT_BasePlayer", "m_vecVelocity[0]"),
            [1] = se.get_netvar("DT_BasePlayer", "m_vecVelocity[1]")
        }
    
        velocity = math.sqrt(localPlayer:get_prop_float(m_vecVelocity[0]) ^ 2 + localPlayer:get_prop_float(m_vecVelocity[1]) ^ 2)

        if velocity ~= nil then
            if velocity > 5 then
                ui.get_check_box("misc_autostrafer"):set_value(true)
            else
                ui.get_check_box("misc_autostrafer"):set_value(false)
            end
        else
            ui.get_check_box("misc_autostrafer"):set_value(true)
        end

    else

        velocity = nil

    end
  end
end


client.register_callback("paint", jumpscot)

local function hidereaader()
    if hideradr:get_value() and engine.is_connected() then
        local convar = se.get_convar("sv_disable_radar")
        convar:set_int(1)
    else
        local convar = se.get_convar("sv_disable_radar")
        convar:set_int(0)
    end
end


client.register_callback("paint", hidereaader)


local function pingspikes()
    if pingspke:get_value() and engine.is_connected() then
local psval = ps_slider:get_value()

if pskey:is_active() then

    ui.get_slider_int("misc_ping_spike_amount"):set_value(psval)
else
    ui.get_slider_int("misc_ping_spike_amount"):set_value(0)
   end
end
end

client.register_callback("paint", pingspikes)

-- anti aim stuff

local enemy_close = function()
    if aantibc:get_value() and engine.is_connected() then
    local close = false

    local enemy = entitylist.get_players(0)
    local local_player = entitylist.get_local_player()
    local lpp = local_player:get_player_hitbox_pos(3)

    local knifes = {41, 42, 59, 80, 505, 506, 507, 508, 509, 512, 514, 515, 516, 519, 520, 522, 523, 503, 517, 518, 521, 525}

    for i = 1, #enemy do
        if enemy[i]:is_alive() then
            local awh = enemy[i]:get_prop_int(se.get_netvar('DT_BaseCombatCharacter', 'm_hActiveWeapon'))
            local aw = entitylist.get_entity_from_handle(awh)
            local ep = enemy[i]:get_player_hitbox_pos(3)
            local d = {ep.x - lpp.x, ep.y - lpp.y, ep.z - lpp.z}
            for i = 1, #knifes do
                local knife = knifes[i]
                if aw:get_prop_int(se.get_netvar('DT_BaseAttributableItem', 'm_iItemDefinitionIndex')) == knife then
                    if math.floor(math.sqrt(d[1]^2 + d[2]^2 + d[3]^2)) <= 120 then
                        close = true
                    end
                end
            end
        end
    end
    return close
end
end

local yaw, pitch = 1, 1

client.register_callback('create_move', function()
    if aantibc:get_value() and engine.is_connected() then
    if ui.get_combo_box('antihit_antiaim_yaw'):get_value() ~= 0 and ui.get_combo_box('antihit_antiaim_pitch'):get_value() ~= 0 then
        yaw = ui.get_combo_box('antihit_antiaim_yaw'):get_value()
        pitch = ui.get_combo_box('antihit_antiaim_pitch'):get_value()
    end

    if enemy_close() then
        ui.get_combo_box('antihit_antiaim_yaw'):set_value(0)
        ui.get_combo_box('antihit_antiaim_pitch'):set_value(0)
    else
        ui.get_combo_box('antihit_antiaim_yaw'):set_value(yaw)
        ui.get_combo_box('antihit_antiaim_pitch'):set_value(pitch)
    end
end
    
end)

local IClientEntityList = ffi.cast("void***", se.create_interface("client.dll", "VClientEntityList003"))
local GetClientEntity = ffi.cast("void*(__thiscall*)(void*, int)", IClientEntityList[0][3])
function entity_t:get_address()
    data = GetClientEntity(IClientEntityList, self:get_index())
	return tonumber(string.sub(tostring(data), 16, -1))
end

local player_vtable = ffi.cast("int*", client.find_pattern("client.dll", "55 8B EC 83 E4 F8 83 EC 18 56 57 8B F9 89 7C 24 0C") + 0x47)[0];
local get_abs_origin = ffi.cast("float*(__thiscall*)(int)", ffi.cast("int*", player_vtable + 0x28)[0]);


local antihit_accurate_walk_bind = ui.get_key_bind("antihit_accurate_walk_bind")
local antiaim_yaw = ui.get_combo_box("antihit_antiaim_yaw")

local lua_fs_antiaim_side_yaw = 1


local m_iHealth = se.get_netvar("DT_BasePlayer", "m_iHealth")

local function freestand()
    local local_player = entitylist.get_local_player()
    local local_player_yaw = engine.get_view_angles().yaw
	local me = entitylist.get_entity_by_index(local_player)
    local abs_origin = get_abs_origin(local_player:get_address()) 
    local fraction_number = {
        ["left"] = 0,
        ["right"] = 0
    }
    
    for i = local_player_yaw - 90, local_player_yaw + 90, 30 do
        if i ~= local_player_yaw then
            local destination = vec3_t.new(abs_origin[0] + 256 * math.cos(math.rad(i)), abs_origin[1] + 256 * math.sin(math.rad(i)), abs_origin[2])
            local trace = trace.line(engine.get_local_player(), 0x46004003, vec3_t.new(abs_origin[0], abs_origin[1], abs_origin[2]), destination)
            local side = i < local_player_yaw and "left" or "right"

            fraction_number[side] = fraction_number[side] + trace.fraction
        end
    end

    lua_fs_antiaim_side_yaw = fraction_number["left"] > fraction_number["right"] and 2 or 3
	
	if lua_fs_yaw:is_active() then
		antiaim_yaw:set_value(lua_fs_antiaim_side_yaw)
	else
		antiaim_yaw:set_value(1)
	end
	
end

client.register_callback("paint", freestand)

client.register_callback("paint", arrowsn)

local function arrowsn()
    local flipbind = ui.get_key_bind("antihit_antiaim_flip_bind")
    local flipbindactive = flipbind:is_active()
    local colorarrows = arrowscolor:get_value()
    --#1
  if engine.is_connected() and arrowscobmo:get_value() == 1 and arrowsenab:get_value() then

    renderer.text("R", actasym, vec2_t.new(screensize.x/2 + 49, screensize.y/2 -9), 23, color_t.new(17, 17, 17, 100))
    renderer.text("Q", actasym, vec2_t.new(screensize.x/2 - 69, screensize.y/2 -9), 23, color_t.new(17, 17, 17, 100))
    if manualleft:is_active() then
        renderer.text("Q", actasym, vec2_t.new(screensize.x/2 - 69, screensize.y/2 -9), 23, colorarrows)
    end
    if manualright:is_active() then
        renderer.text("R", actasym, vec2_t.new(screensize.x/2 + 49, screensize.y/2 -9), 23, colorarrows)
    end
    
    if flipbindactive then
       renderer.text("|", veractasym, vec2_t.new(screensize.x/2 - 53, screensize.y/2 -11), 21, color_t.new(17, 17, 17, 100))
       renderer.text("|", veractasym, vec2_t.new(screensize.x/2 + 43, screensize.y/2 -11), 21, colorarrows)
    else
        renderer.text("|", veractasym, vec2_t.new(screensize.x/2 - 53, screensize.y/2 -11), 21, colorarrows)
        renderer.text("|", veractasym, vec2_t.new(screensize.x/2 + 43, screensize.y/2 -12), 21, color_t.new(17, 17, 17, 100))
end
end

--#2
if engine.is_connected() and arrowscobmo:get_value() == 2 and arrowsenab:get_value() then

    if manualleft:is_active() then
        renderer.text("<", veractasym, vec2_t.new(screensize.x/2 - 60, screensize.y/2 -11), 20, colorarrows)
    end
    if manualright:is_active() then
        renderer.text(">", veractasym, vec2_t.new(screensize.x/2 + 50, screensize.y/2 -11), 20, colorarrows)
    end

    if flipbindactive then
        renderer.text("<", veractasym, vec2_t.new(screensize.x/2 - 68, screensize.y/2 -11), 20, color_t.new(17, 17, 17, 100))
        renderer.text(">", veractasym, vec2_t.new(screensize.x/2 + 58, screensize.y/2 -11), 20, colorarrows)
     else
         renderer.text("<", veractasym, vec2_t.new(screensize.x/2 - 68, screensize.y/2 -11), 20, colorarrows)
         renderer.text(">", veractasym, vec2_t.new(screensize.x/2 + 58, screensize.y/2 -11), 20, color_t.new(17, 17, 17, 100))
 end
end

end

client.register_callback("paint", arrowsn)
